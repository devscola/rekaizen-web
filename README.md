### Install

```sh
npm install
```

### Run development

```sh
npm start
npm run server
```

### Build CSS

```sh
npm run css-build
```
